package com.projeto.projetoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ProjetointApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetointApplication.class, args);
    }

}
