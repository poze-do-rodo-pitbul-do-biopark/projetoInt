package com.projeto.projetoint.controller;

import com.projeto.projetoint.domain.fornecedor.Fornecedor;
import com.projeto.projetoint.records.RequestFornecedor;
import com.projeto.projetoint.repositories.FornecedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/fornecedor")
public class FornecedorController {
    @Autowired
    private FornecedorRepository repository;

    @GetMapping
    public ResponseEntity getFornecedor() {
        var todosFornecedores = repository.findAllByActiveTrue();
        return ResponseEntity.ok(todosFornecedores);
    }

    @PostMapping
    public ResponseEntity registrarFornecedor(@RequestBody @Validated RequestFornecedor data) {
        Fornecedor newFornecedor = new Fornecedor(data);
        repository.save(newFornecedor);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity updtFornecedor(@RequestBody @Validated RequestFornecedor data) {
        Optional<Fornecedor> optionalFornecedor = repository.findById(data.id_fornecedor());
        if (optionalFornecedor.isPresent()) {
            Fornecedor fornecedor = optionalFornecedor.get();
            fornecedor.setNome_fornecedor(data.nome_fornecedor());
            fornecedor.setCnpj_fornecedor(data.cnpj_fornecedor());
            fornecedor.setEmail_fornecedor(data.email_fornecedor());
            repository.save(fornecedor);
            return ResponseEntity.ok().body(fornecedor);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteFornecedor(@PathVariable Integer id) {
        Optional<Fornecedor> optionalFornecedor = repository.findById(id);

        if (optionalFornecedor.isPresent()) {
            Fornecedor fornecedor = optionalFornecedor.get();
            fornecedor.setActive(false);
            repository.save(fornecedor);
            return ResponseEntity.ok().body(fornecedor);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}

