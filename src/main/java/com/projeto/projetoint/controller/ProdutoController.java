package com.projeto.projetoint.controller;


import com.projeto.projetoint.domain.produto.Produto;
import com.projeto.projetoint.records.RequestProduto;
import com.projeto.projetoint.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
    @Autowired
    private ProdutoRepository repository;

    @GetMapping
    public ResponseEntity getProdutos() {
        var todosProdutos = repository.findAllByActiveTrue();
        return ResponseEntity.ok(todosProdutos);
    }

    @PostMapping
    public ResponseEntity registrarProduto(@RequestBody @Validated RequestProduto data) {
        Produto newProduto = new Produto(data);
        repository.save(newProduto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity updtProduto(@RequestBody @Validated RequestProduto data) {
        Optional<Produto> optionalProduto = repository.findById(data.id_produto());

        if (optionalProduto.isPresent()) {
            Produto produto = optionalProduto.get();
            produto.setNome_produto(data.nome_produto());
            produto.setDesc_produto(data.desc_produto());
            produto.setQuant_min(data.quant_min());
            produto.setValor_venda(data.valor_venda());
            repository.save(produto);
            return ResponseEntity.ok().body(produto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduto(@PathVariable Integer id) {
        Optional<Produto> optionalProduto = repository.findById(id);

        if (optionalProduto.isPresent()) {
            Produto produto = optionalProduto.get();
            produto.setActive(false);
            repository.save(produto);
            return ResponseEntity.ok().body(produto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}

