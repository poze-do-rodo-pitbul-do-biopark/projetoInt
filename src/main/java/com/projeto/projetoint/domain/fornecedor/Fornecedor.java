package com.projeto.projetoint.domain.fornecedor;


import com.projeto.projetoint.records.RequestFornecedor;
import jakarta.persistence.*;
import lombok.*;

@Table(name = "fornecedor")
@Entity(name = "fornecedor")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id_fornecedor")
public class Fornecedor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_fornecedor;
    private String nome_fornecedor;
    private String cnpj_fornecedor;
    private String email_fornecedor;
    private Boolean active;

    public Fornecedor(RequestFornecedor requestFornecedor) {
        this.nome_fornecedor = requestFornecedor.nome_fornecedor();
        this.cnpj_fornecedor = requestFornecedor.cnpj_fornecedor();
        this.email_fornecedor = requestFornecedor.email_fornecedor();
        this.active = true;
    }
}



