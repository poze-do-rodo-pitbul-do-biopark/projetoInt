package com.projeto.projetoint.domain.produto;

import com.projeto.projetoint.records.RequestProduto;
import jakarta.persistence.*;
import lombok.*;

@Table(name = "produto")
@Entity(name = "produto")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id_produto")
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_produto;
    private String nome_produto;
    private String desc_produto;
    private int quant_min;
    private Float valor_venda;
    private Boolean active;

    public Produto(RequestProduto requestProduto) {
        this.nome_produto = requestProduto.nome_produto();
        this.desc_produto = requestProduto.desc_produto();
        this.quant_min = requestProduto.quant_min();
        this.valor_venda = requestProduto.valor_venda();
        this.active = true;
    }

}

