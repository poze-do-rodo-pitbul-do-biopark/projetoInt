package com.projeto.projetoint.records;


public record RequestProduto(
        int id_produto,

        String nome_produto,

        String desc_produto,

        Integer quant_min,

        Float valor_venda) {
}


