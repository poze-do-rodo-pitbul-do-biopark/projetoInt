package com.projeto.projetoint.repositories;

import com.projeto.projetoint.domain.produto.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
    List<Produto> findAllByActiveTrue();
}
