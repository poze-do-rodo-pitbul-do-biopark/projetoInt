CREATE TABLE produto
(
    id_produto   INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nome_produto VARCHAR(255) NOT NULL,
    desc_produto VARCHAR(500) NOT NULL,
    quant_min    INT          NOT NULL,
    valor_venda DOUBLE NOT NULL
);

