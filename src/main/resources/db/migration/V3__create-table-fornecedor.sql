CREATE TABLE fornecedor
(
    id_fornecedor    INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nome_fornecedor  VARCHAR(255) NOT NULL,
    cnpj_fornecedor  VARCHAR(500) NOT NULL,
    email_fornecedor VARCHAR(100) NOT NULL
);