var formulario = document.getElementById("cadastro");
document.querySelector("#mostrar").addEventListener("click", () => {
    formulario.classList.toggle("visivel")

    document.getElementById("fundoEscuro").style.display = "block";
});

document.addEventListener('DOMContentLoaded', () => {
    const INPUT_BUSCA = document.getElementById('input-busca');
    const FILTRAR_TABELA = document.getElementById('filtragem-tabela');

    INPUT_BUSCA.addEventListener('keyup', () => {
        let expressao = INPUT_BUSCA.value.toLowerCase();

        if (expressao.length === 1) {
            return;
        }

        let linhas = FILTRAR_TABELA.getElementsByTagName('tr');

        for (let posicao = 0; posicao < linhas.length; posicao++) {
            let conteudoDaLinha = linhas[posicao].innerHTML.toLowerCase();

            if (conteudoDaLinha.includes(expressao)) {
                linhas[posicao].style.display = '';
            } else {
                linhas[posicao].style.display = 'none';
            }
        }
    });
});

